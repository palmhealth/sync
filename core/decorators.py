from functools import wraps


def athena_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        if response.status_code < 300:
            return response.json()
        # Error
        else:
            print('Error in {} with args. status: {}. Error: {}'.format(
                  func.__name__, response.status_code, response.content))
    return wrapper


def output_list_first(func):
    def wrapper(*args, **kwargs):
        items = func(*args, **kwargs)
        return items[0] if isinstance(items, list) and len(items) > 0 else None
    return wrapper


def output_list_object(func):
    def wrapper(*args, **kwargs):
        body = func(*args, **kwargs)
        if isinstance(body, list) and len(body):
            return body[0]
    return wrapper


def output_object_key(obj_key):
    def param(func):
        def wrapper(*args, **kwargs):
            body = func(*args, **kwargs)
            if body and obj_key in body:
                return body[obj_key]
            else:
                raise Exception('the key "{0}" does not exist for response '
                'from {1} with args {2}. Response: {3}'.format(obj_key,
                                  func.__closure__[0].cell_contents.__name__,
                                  args,
                                  body))
        return wrapper
    return param


def apply_func(f):
    def param(func):
        def wrapper(*args, **kwargs):
            response = func(*args, **kwargs)
            return f(response)
        return wrapper
    return param
