from unittest import TestCase

from . import storage


class PalmTestCase(TestCase):
    def tearDown(self):
        storage.drop_all()
