import json
import datetime

import pytz as tz

from core import config
from core import synchronizer
from core import storage

now = datetime.datetime.utcnow()

# MindBody works in Central Time
cttz = tz.timezone(config.site_config["timezone"])


latest_sync = storage.syncs.find_one(order_by="-created_at")
if latest_sync:
    since = cttz.fromutc(latest_sync.get("created_at"))
else:
    since = cttz.fromutc(now) - datetime.timedelta(minutes=15)
# Build since and until from MindBody API call
# ensure that the time on the sync overlaps the time on MB system.
since = since - datetime.timedelta(minutes=2)
since = since.strftime("%Y-%m-%d %H:%M:%S")
until = cttz.fromutc(now).strftime("%Y-%m-%d %H:%M:%S")

# Call synchronization scripts
print(
    'exporting patients for env "%s", site "%s", from %s until %s'
    % (config.ENV, config.SITE_NAME, since, until)
)
created_ok, created_fail = synchronizer.sync_created_patients(since, until)
(
    upcreated_ok,
    updated_ok,
    upcreated_fail,
    updated_fail,
) = synchronizer.sync_updated_patients(since, until)

# Outputting for info purpose
print("- patients sync -")
print("created: %d, failed: %d" % (len(created_ok), len(created_fail)))
print(
    "updated: %d, upcreated: %d, create failed: %d, update fail: %d"
    % (len(updated_ok), len(upcreated_ok), len(created_fail), len(updated_fail))
)


# Call synchronization appointments
print(
    'exporting appointments for env "%s", site "%s", from %s until %s'
    % (config.ENV, config.SITE_NAME, since, until)
)


#  Sync appointments  #

created_ok, created_fail = synchronizer.sync_created_appointments(since, until)
print("Appointments: created: {}, create failed: {}".format(created_ok, created_fail))


# -- Deactivated appointment updates. Athena does not permit cancelling.
# -- and registering the client's arrival creates duplicates.
# upcreated_ok, updated_ok, upcreated_fail, updated_fail = (
#     synchronizer.sync_updated_appointments(since, until) or [], [], [], []
# )

# print('Appointments: updated: %d, upcreated: %d, create failed: %d, '
#       'update fail: %d' % (len(updated_ok), len(upcreated_ok),
#                            len(created_fail), len(updated_fail))
#       )


# Save synchronization in history
stored = storage.syncs.insert(
    {
        "created_at": now,
        "ids": json.dumps(
            {"created": created_ok + upcreated_ok, "updated": updated_ok, "deleted": []}
        ),
        "failed": json.dumps(
            {
                "created": created_fail + upcreated_fail,
                "updated": updated_fail,
                "deleted": [],
            }
        ),
    }
)

print("udpated sync id {} ---".format(stored))
