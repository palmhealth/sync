import phonenumbers


def ucfirst(text):
    return ' '.join([p.capitalize() for p in text.lower().split(' ')])


def clean_str(string):
    return string.strip() if string else ''


def extract_us_number(number):
    """
    Converts a number into a US phone number if compatible.
    Reject dummy numbers like 8888888888, 12333333 or other that don't have more
    than 3 different digits.
    """
    try:
        phone = phonenumbers.parse(clean_str(number), 'US')
    except phonenumbers.phonenumberutil.NumberParseException:
        return None
    if (phonenumbers.is_valid_number_for_region(phone, 'US') and
            len(set(str(phone.national_number))) > 3):
        return str(phone.national_number)
