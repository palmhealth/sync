from faker import Factory
from core.apis.mindbody import date_to_string


def fake_appointment_wellbeing():
    return {
        'ApptID': '1001',
        'BookedDate': '5/19/2022 12:54:03 PM',
        'CancelDate': None,
        'Completed': 'No',
        'Confirmed': 'No',
        'EarlyCancel': 'No',
        'EmployeeFirst': 'Julie G.',
        'EmployeeLast': 'Geeting',
        'FirstName': 'Richard',
        'LastModified': None,
        'LastName': 'Rogers',
        'LateCancel': 'No',
        'LocationName': 'PALM Health',
        'MethodBooked': None,
        'PricingOptionApplied': None,
        'PricingOptionName': None,
        'ProviderID': None,
        'RSSID': 'PH10254',
        'RegionID': '917',
        'ServiceCategory': 'Coaching',
        'SignedIn': 'No',
        'StaffID': '21',
        'StudioID': '334223',
        'SystemID': '2287',
        'VisitDate': '5/19/2022 12:00:00 AM',
        'VisitEnd': '12/30/1899 8:30:00 AM',
        'VisitNotes': None,
        'VisitTime': '12/30/1899 8:00:00 AM',
        'VisitTypeID': '129',
        'VisitTypeName': 'Assessment - Well-Being and Stress',
        'row': '1'
    }


def fake_appointment_medical():
    fake_appointment = fake_appointment_wellbeing()
    fake_appointment['ApptID'] = '1002'
    fake_appointment['ServiceCategory'] = 'Mental health'
    fake_appointment['StaffID'] = '11'
    fake_appointment['VisitTypeID'] = '172'
    fake_appointment['VisitTypeName'] = 'Mental Health Initial'
    fake_appointment['EmployeeFirst'] = 'Nigel'
    fake_appointment['EmployeeLast'] = 'Lester'
    return fake_appointment


def fake_client():
    '''
    Generates a mindbody formatted client object.
    '''
    faker = Factory.create()
    return {
        'SiteID': -99,
        'SiteName': faker.word(),
        'HomeStudioName': faker.word(),
        'HomeStudioAddress': faker.street_address(),
        'HomeStudioCity': faker.city(),
        'HomeStudioState': faker.state_abbr(),
        'HomeStudioCountry': faker.country_code(),
        'HomeStudioLatitude': faker.latitude(),
        'HomeStudioLongitude': faker.longitude(),
        'ClientBarcodeID': faker.ean8(),
        'ClientSystemID': faker.ean8(),
        'ClientFirstName': faker.first_name(),
        'ClientLastName': faker.last_name(),
        'ClientNickname': faker.last_name(),
        'ClientAddress': faker.street_address(),
        'ClientAddressLn2': faker.street_suffix(),
        'ClientCity': faker.city(),
        'ClientState': faker.state_abbr(),
        'ClientZip': faker.zipcode(),
        'ClientCountry': faker.country_code(),
        'ClientLatitude': faker.latitude(),
        'ClientLongitude': faker.longitude(),
        'ClientBirthDate': date_to_string(faker.date_time_between(
            start_date='-99y', end_date='-20y')),
        'Gender': faker.random_element(elements=('Male', 'Female')),
        'ClientEmail': faker.email(),
        'ClientEmailOptIn': faker.random_element(elements=(True, False)),
        'ClientPromoOptIn': faker.random_element(elements=(True, False)),
        'ClientHomePh': faker.phone_number(),
        'ClientCell': faker.phone_number(),
        'ClientWorkPh': faker.phone_number(),
        'ClientActive': faker.random_element(elements=(True, False)),
        'ClientDeactivatedDateTime': None,
        'Prefix': 'Jr',
        'CreationDate': date_to_string(faker.date_time_between(
            start_date='-15m', end_date='now')),
        'ClientIsCompany': False,
        'ClientIsProspect': False,
        'ClientReferrer': None,
        'ReferrerID': None,
        'ReferringClientName': None,
        'ClientCreatedBy': None,
        'ClientCreationMethod': 'Client',
        'ClientStatus': None,
        'ClientMembership': None,
        'ClientPreviousStatus': 'Active',
        # 'StatusChangeDate': '10/12/2013 4:49:10 AM',
        'EmailOptIn': True,
        'PromoOptIn': True,
        'EthnicityCode': None,
        'ProfessionalProvider': None,
        'row': 1,
    }
