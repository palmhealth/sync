#!/usr/bin/env python
import sys
import os

sys.path.append(os.path.dirname(__file__) + '/../')

import json
import csv

from core.storage import patients


z_columns = ['GuestId', 'Code']
a_columns = ['firstname', 'middlename', 'lastname', 'sex', 'dob', 'email',
             'homephone', 'mobilephone', 'address1', 'address2', 'zip', 'city',
             'state', 'countrycode3166']

outputs = []
for patient in patients.all():
    a_data = json.loads(patient['athena_data'])
    z_data = json.loads(patient['zenoti_data'])
    row = []
    for prop in z_columns:
        row.append(z_data[prop])
    for prop in a_columns:
        row.append(a_data[prop])
    outputs.append(row)

target = open('patients.csv', 'w')
writer = csv.writer(target)
writer.writerows([z_columns + a_columns] + outputs)
