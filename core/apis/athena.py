import datetime

import requests

from .. import config
from ..decorators import (output_list_first, output_list_object, apply_func,
                          output_object_key, athena_response)
from core import storage, synchronizer  # , logging
from core.apis import mindbody
from core.utils import clean_str, extract_us_number


athena_config = config.ATHENA[config.ENV]


def patient_from_mindbody(client):
    birthday = client.get('ClientBirthDate') or '01/01/1920'
    dob = datetime.datetime.strftime(
        mindbody.date_from_string(birthday), '%m/%d/%Y'
    )
    return {
        'departmentid': athena_config['department_id'],
        'dob': dob,
        'email': clean_str(client.get('ClientEmail')) or '_',
        'lastname': clean_str(client['ClientLastName']),
        'firstname': clean_str(client['ClientFirstName']),
        'middlename': clean_str(client.get('ClientMiddleName', '')),
        'address1': clean_str(client.get('ClientAddress', '')),
        'address2': clean_str(client.get('ClientAddressLn2', '')),
        'city': clean_str(client.get('ClientCity', '')),
        'zip': clean_str(client.get('ClientZip', '')),
        'state': client.get('ClientState', ''),
        'countrycode': client.get('ClientCountry', ''),
        'countrycode3166': client.get('ClientCountry', ''),
        'mobilephone': extract_us_number(client.get('ClientCell', '')) or '',
        'homephone': (extract_us_number(client.get('ClientHomePh', '')) or
                      extract_us_number(client.get('ClientWorkPh', '')) or ''),
        'sex': (clean_str(client.get('Gender', '')).lower()[0]
                if client.get('Gender', '') in ('Male', 'Female') else ''),
        'suffix': clean_str(client.get('Prefix', '')),
        # Unsure what to do with these from MindBody:
        # 'ClientCreatedBy': None,
        # 'ClientCreationMethod': 'Client',
        # 'ClientStatus': 'Expired',
    }


def retrieve_token(config):
    """
    Retrieves an access_token.
    """
    url = config['token_url']
    payload = {
        'grant_type': 'client_credentials',
        'scope': 'athena/service/Athenanet.MDP.*',
    }
    auth = requests.auth.HTTPBasicAuth(config['key'],
                                       config['secret'])
    response = requests.post(url, data=payload, auth=auth)
    if response.status_code == 401:
        raise Exception('Check your Athena token credentials for current '
                        'config.')

    token = response.json()['access_token']
    return token

try:
    token = retrieve_token(athena_config)
except requests.exceptions.ConnectionError as e:
    token = ''
    print('You seem not to be connected:', e)

default_headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Bearer {token}'.format(token=token),
    'Accept-Encoding': None,
}


@athena_response
def get_practice_infos():
    """This function is here mainly for testing purpose."""
    url = '{url}1/practiceinfo'.format(url=athena_config['url'])
    return requests.get(url, headers=default_headers)


def athena_url(uri):
    return '{url}{practice_id}/{uri}'.format(
        url=athena_config['url'],
        practice_id=athena_config['practice_id'],
        uri=uri
    )


@output_list_object
@athena_response
def get_patient(patient_id):
    """
    Retrieves one patient based on his athena patientid.
    """
    url = athena_url('patients/{0}'.format(patient_id))
    return requests.get(url, headers=default_headers)


@output_object_key('patients')
@athena_response
def find_patients(data):
    '''
    Finds a patient based on multiple criterias. Uses GET '/patients' API call.
    '''
    url = athena_url('patients')
    return requests.get(url, params=data, headers=default_headers)


@output_object_key('patientid')
@output_list_object
@athena_response
def create_patient(data):
    """
    Exports a patient to Athena.
    """
    url = athena_url('patients')
    return requests.post(url, data=data, headers=default_headers)


@output_object_key('patientid')
@output_list_object
@athena_response
def update_patient(id, data):
    """
    Updates an existing patient on Athena.
    param id: Athena patient id to update
    param data: Athena formatted object with new data
    """
    url = athena_url('patients/{patient_id}'.format(patient_id=id))
    return requests.put(url, params=data, headers=default_headers)


def delete_patient(id):
    """
    Deletes an existing patient on Athena.
    param id: Athena patient id to delete.
    """
    return update_patient(id, {'status': 'deleted'})


@output_object_key('providers')
@athena_response
def get_providers():
    """
    Retrieves providers (doctors) from Athena.
    """
    url = athena_url('providers')
    return requests.get(url, headers=default_headers)


def export_appointment(data):
    """
    Exports an appointment to Athena.
    """
    url = athena_url('appointments/open')
    return requests.post(url, data=data, headers=default_headers)


@output_list_object
@athena_response
def get_appointment(appointment_id):
    url = athena_url('appointments/{0}'.format(appointment_id))
    return requests.get(url, headers=default_headers)


@output_object_key('appointments')
@athena_response
def get_appointment_slots(data):
    url = athena_url('appointments/open')
    data.update({
        'departmentid': athena_config['department_id'],
    })
    return requests.get(url, params=data, headers=default_headers)


@output_list_first
@athena_response
def get_appointment_slot(slot_id):
    url = athena_url('appointments/{0}'.format(slot_id))
    data = {
        'departmentid': athena_config['department_id'],
    }
    return requests.get(url, params=data, headers=default_headers)


@output_object_key('appointments')
@athena_response
def get_appointments_for_patient(patient_id):
    url = athena_url('patients/{0}/appointments'.format(patient_id))
    data = {
        'departmentid': athena_config['department_id'],
    }
    return requests.get(url, params=data, headers=default_headers)


@apply_func(lambda x: int(list(x.keys())[0]))
@output_object_key('appointmentids')
@athena_response
def create_appointment_slot(data):
    url = athena_url('appointments/open')
    data.update({
        'departmentid': athena_config['department_id'],\
    })
    return requests.post(url, data=data, headers=default_headers)


@apply_func(lambda x: int(x))
@output_object_key('appointmentid')
@output_list_first
@athena_response
def create_appointment_booking(slot_id, patient_id, type_id, booking_note):
    url = athena_url('appointments/{0}'.format(slot_id))
    data = {
        'departmentid': athena_config['department_id'],
        'patientid': patient_id,
        'appointmenttypeid': type_id,
        'bookingnote': booking_note,
        'ignoreschedulablepermission': True,
    }
    return requests.put(url, data=data, headers=default_headers)


def create_appointment(data):
    """
    Creates an appointment slot and books the appointment at the same time.
    """
    slot_id = create_appointment_slot(data)
    appt_id = create_appointment_booking(slot_id=slot_id,
                                         patient_id=data['patientid'],
                                         type_id=data['appointmenttypeid'],
                                         booking_note=data['bookingnote'])
    return int(appt_id)


def update_appointment(appointment_id, data):
    """
    Update an appointment
    """
    cancel_appointment(appointment_id)
    return create_appointment(data)


@apply_func(lambda x: x.get('status', None) == 'x')
@athena_response
def cancel_appointment(appointment_id, cancel_date=None):
    '''
    Returns `True` object if the appointment was cancelled.
    '''
    appointment = get_appointment(appointment_id)
    url = athena_url('appointments/{appt_id}/cancel'.format(
        appt_id=appointment_id))
    if cancel_date is None:
        cancel_date = datetime.datetime.utcnow().strftime('%m/%d/%Y')
    data = {
        'patientid': appointment['patientid'],
        'cancellationreason': 'Cancelation from Mindbody : ' + cancel_date
    }
    return requests.put(url, params=data, headers=default_headers)


@output_object_key('appointmentid')
@output_list_object
@athena_response
def delete_appointment(appointment_id):
    cancel_appointment(appointment_id)
    url = athena_url('appointments/{0}'.format(appointment_id))
    return requests.delete(url, headers=default_headers)


def appointment_from_mindbody(appt):
    appt_id = appt['ApptID']
    appt_type_id = dict(synchronizer.app_type_ids).get(int(appt['VisitTypeID']))
    duration = mindbody.get_appointment_duration(appt)
    date = mindbody.date_from_string(appt['VisitDate'])
    time = mindbody.date_from_string(appt['VisitTime'])
    patient = storage.patients.find_one(mindbody_id=appt['SystemID'])
    if not patient:
        raise AttributeError('No patient exist with mindbody_id {}'.format(
                             appt['SystemID']))
    provider_id = (dict(synchronizer.provider_ids).get(int(appt['StaffID'])) or
                   71)
    if not patient:
        print('Client {patient} does not exist for appointment on {date} '
              '{time}!'.format(patient=appt['LastName'], date=date, time=time))
#   logging.error(appointment, message=message)
    if not provider_id:
        print('Provider {StaffID} (or ProviderID?) does not exist for '
              'appointment on {date} {time}! Defaulting providerid to '
              '"{provider}"'.format(StaffID=appt['StaffID'], date=date,
                                    time=time, provider=provider_id))
    try:
        patient['athena_id']
    except:
        pass
    if appt['CancelDate'] is not None:
        appointmentstatus = 'x'
    else:
        appointmentstatus = 'f'
    end_appointment_time = mindbody.date_from_string(appt['VisitEnd'])
    end_time = 'Ends at: ' + end_appointment_time.strftime('%-I:%M%p')
    data = {
        'patientid': int(patient.get('athena_id')),
        'date': date.strftime('%m/%d/%Y'),
        'starttime': time.strftime('%H:%M'),
        'bookingnote': end_time,
        'duration': duration,
        'appointmenttypename': appt['VisitTypeName'],
        'departmentid': int(athena_config['department_id']),
        'appointmenttype': appt['ServiceCategory'],
        'appointmenttypeid': appt_type_id,
        'appointmentstatus': appointmentstatus,  # 'Confirmed': 'No'
        'providerid': provider_id,
    }
    return data


def export_appointment_format(data):
    formatted = data
    formatted['appointmentdate'] = formatted.pop('date')
    formatted['appointmenttime'] = formatted.pop('starttime')
    return formatted
