import sys
import os
import json

sys.path.append(os.path.dirname(__file__) + '/../')

from core import storage
from core.storage import patients

if len(sys.argv) != 3:
    # TODO we can improve this script by adding getopt
    print('Usage : python find_patient first_name last_name')
    exit(1)

# Get all patients of storage
for patient in patients.all():
    # If the patient are found we print it
    if storage.search_patient(patient, sys.argv[1], sys.argv[2]):
        try:
            # Because datetime are are not serializable
            print(json.dumps(dict(patient), indent=4, sort_keys=True))
        except TypeError:
            print(dict(patient))
