import json
from datetime import datetime
from decimal import Decimal

import dataset

from core import config
from core.apis import athena, mindbody


db = dataset.connect(config.DATABASE[config.ENV]['path'])
patients = db['patients']
syncs = db['syncs']
providers = db['providers']
appointments = db['appointments']
failures = db['failures']


def create_patient(client, athena_id):
    athena_data = athena.patient_from_mindbody(client)
    client = {k: (float(v) if isinstance(v, Decimal) else v)
              for (k, v) in client.items()}
    return patients.insert({
        'created_at': datetime.utcnow(),
        'updated_at': None,
        'mindbody_id': int(client['ClientSystemID']),
        'athena_id': int(athena_id),
        'athena_data': json.dumps(athena_data),
        'mindbody_data': json.dumps(client),
        'deleted': False,
    })


def update_patient(id, client):
    athena_data = athena.patient_from_mindbody(client)
    return patients.update({
        'id': id,
        'updated_at': datetime.utcnow(),
        'mindbody_data': json.dumps(client),
        'athena_data': json.dumps(athena_data),
    }, ['id'])


# Return true if patient exist in the database, false otherwise
def search_patient(patient, first_name, last_name):
    if patient['athena_data'] is not None:
        # Check for mindbody
        if patient['mindbody_data'] is not None:
            json_patient_mb = json.loads(patient['mindbody_data'])
            if (json_patient_mb['ClientFirstName'] == first_name and
                    json_patient_mb['ClientLastName'] == last_name):
                return True
        # Check for athena
        json_patient_athena = json.loads(patient['athena_data'])
        if (json_patient_athena['firstname'] == first_name and
                json_patient_athena['lastname'] == last_name):
            return True
        else:
            return False


def create_appointment(mindbody_data, athena_data, athena_id):
    return appointments.insert({
        'created_at': datetime.utcnow(),
        'updated_at': None,
        'mindbody_id': mindbody_data['ApptID'],
        'athena_id': int(athena_id),
        'athena_data': json.dumps(athena_data),
        'mindbody_data': json.dumps(mindbody_data),
        'deleted': False,
    })


def update_appointment(id, mindbody_data, athena_data, athena_id):
    return appointments.update({
        'id': id,
        'updated_at': datetime.utcnow(),
        'athena_id': int(athena_id),
        'athena_data': json.dumps(athena_data),
        'mindbody_data': json.dumps(mindbody_data),
        'deleted': False,
    }, ['id'])


def delete_appointment(id):
    return appointments.update({
        'id': id,
        'deleted': True,
    }, ['id'])


def create_provider(mindbody_id, athena_id, **kwargs):
    return providers.insert({
        'created_at': datetime.utcnow(),
        'updated_at': None,
        'mindbody_id': 21,
        'athena_id': int(athena_id),
        'firstname': kwargs['firstname'],
        'lastname': kwargs['lastname'],
    })

def create_failure(data, forced=None):
    return failures.insert({
        'created_at': datetime.utcnow(),
        'mindbody_data': data,
        'forced': forced,
    })


def drop_all():
    try:
        providers.delete()
        patients.delete()
        appointments.delete()
        syncs.delete()
        failures.delete()
    except:
        pass
