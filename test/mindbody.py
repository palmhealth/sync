from datetime import datetime

from core.test import PalmTestCase
from core.apis.mindbody import (
    retrieve_created_clients, retrieve_updated_clients,
    retrieve_created_appointments, retrieve_updated_appointments,
    date_from_string
)


now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
today = datetime.today().strftime('%Y-%m-%d')

client_properties = (
    'ClientSystemID', 'ClientNickname',
    'ClientFirstName', 'ClientLastName',
    'ClientAddress', 'ClientAddressLn2', 'ClientCity', 'ClientState',
    'ClientZip', 'ClientCountry', 'ClientBirthDate', 'Gender',
    'ClientEmail', 'ClientHomePh', 'ClientCell', 'ClientWorkPh',
    'CreationDate', 'ClientCreatedBy'
)


class MindBodyClientTest(PalmTestCase):
    def test_empty_query_results(self):
        results = retrieve_created_clients('1000-01-01', '1000-01-01')
        self.assertEqual(len(results), 0)

    def test_retrieving_created_clients(self):
        clients = retrieve_created_clients('2010-01-01',
                                           today,
                                           limit=1)
        self.assertTrue(len(clients) > 0)
        clients = retrieve_created_clients('2010-01-01 00:00:00',
                                           today + ' 22:00:00',
                                           limit=1)
        self.assertTrue(len(clients) > 0)

    def test_retrieving_updated_clients(self):
        clients = retrieve_updated_clients('2010-01-01', today, limit=1)[0]
        self.assertTrue(len(clients) > 0)
        clients = retrieve_updated_clients('2010-01-01 09:00:00',
                                           today + ' 22:00:00',
                                           limit=1)[0]
        self.assertTrue(len(clients) > 0)

    def test_created_client_format(self):
        client = retrieve_created_clients('2010-01-01', today, limit=1)[0]
        self.assertTrue(all(k in client.keys() for k in client_properties))

    def test_modified_client_format(self):
        client = retrieve_updated_clients('2010-01-01', today, limit=1)[0]
        self.assertTrue(all(k in client.keys() for k in client_properties))


class MindBodyAppointmentTest(PalmTestCase):
    def test_retrieving_created_appointments(self):
        appointments = retrieve_created_appointments('2010-01-01', today,
                                                     limit=1)
        self.assertTrue(len(appointments) > 0)
        appointments = retrieve_created_appointments('2010-01-01 00:00:00',
                                                     today + ' 22:00:00',
                                                     limit=1)
        self.assertTrue(len(appointments) > 0)

    def test_retrieve_create_appointment_date_range(self):
        appointments = retrieve_created_appointments('2017-01-01',
                                                     '2017-01-02',
                                                     limit=1)
        self.assertEqual(len(appointments), 1)
        booked_date_str = appointments[0]['BookedDate']
        start_date = datetime.strptime('2017-01-01', '%Y-%m-%d')
        end_date = datetime.strptime('2017-01-02 23:59:59', '%Y-%m-%d '
                                                            '%H:%M:%S')
        booked_date = date_from_string(booked_date_str)

        self.assertTrue(start_date <= booked_date <= end_date)

    def test_retrieve_update_appointment_date_range(self):
        appointments = retrieve_updated_appointments('2017-01-01',
                                                     '2017-01-02',
                                                     limit=1)
        self.assertEqual(len(appointments), 1)
        last_modified_str = appointments[0]['LastModified']
        start_date = datetime.strptime('2017-01-01', '%Y-%m-%d')
        end_date = datetime.strptime('2017-01-02 23:59:59', '%Y-%m-%d '
                                                            '%H:%M:%S')
        last_modified = date_from_string(last_modified_str)

        self.assertTrue(start_date <= last_modified <= end_date)
