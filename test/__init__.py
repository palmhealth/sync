from unittest import TestCase

from core import config, storage


# Force dev never to run test in prod
config.ENV = 'dev'

from .utils import *
from .mindbody import *
from .athena import *
from .synchronizer import *
from .storage import *
