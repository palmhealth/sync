# -*- encoding: utf-8 -*-

"""
Configuration that is specific to PALM's sites.
"""

STL = {
    "mindbody_site_id": "334223",
    "athena_practice_id": "11898",
    "timezone": "US/Central",
    "app_type_ids": [
        (105, 65),
        (19, 64),  # Initial acupuncture
        (109, 83),
        (110, 82),
        (131, 27),
        (133, 44),
        (130, 25),
        (114, 67),
        (192, 68),
        (113, 66),
        (116, 102),
        (117, 103),
        (118, 77),
        (193, 262),
        (194, 13),
        (176, 57),
        (174, 121),
        (172, 56),
        (120, 78),
        (159, 51),
        (161, 50),
        (157, 49),
        (121, 80),
        (206, 361),
        (225, 201),
        (224, 202),
        (166, 48),
        (164, 47),
        (162, 46),
        (125, 141),
        (126, 23),
        (124, 26),
        (311, 488),
        (364, 528),
        (365, 529),
        (366, 530),
        (303, 468),
        (373, 548),
        (402, 568),
        (346, 588),  # Medical Aesthetic f/u 15 min
        (201, 589),  # Med. Aesth. f/u 30 min
        (416, 590),  # Medical Skin Check
        (200, 591),  # Med. Aesth. Consult initial
        (421, 608),
        (332, 508),
        (303, 468),  # Blood Draw 30 minute
        (10, 65),  # Follow up Acupuncture
        (426, 628),  # Facial Acupuncture
        (427, 629),  # Cupping
        (448, 648),  # CIMT Procedure
        (430, 668),  # Orthotic Initial Consult
        (431, 669),  # Orthotic Fitting
        (432, 670),  # Orthotic Check
        (74, 52),  # MA
        (75, 51),  # Nurse
        (76, 53),  # Lab Visit
        (77, 54),  # IV Suite — IV Nutrition
        (458, 688),  # Glutathione Only IV
        (518, 708),  # Intake Paperwork
        (532, 748),  # Prolozone
        (537, 768),  # CBD Wellness consult
        (191, 788),  # Accugraph Body Balance
        (510, 792),  # elehealth Acute Visit
        (502, 790),  # Telehealth FM/Primary Extended
        (503, 791),  # Telehealth FM/Primary FU
        (501, 789),  # Telehealth FM/Primary Initial
        (559, 803),  # Telehealth Mental Extended
        (560, 806),  # Telehealth Mental FU
        (558, 802),  # Telehealth Mental Initial
        (513, 794),  # Telehealth Neurology Extended
        (514, 795),  # Telehealth Neurology FU
        (512, 793),  # Telehealth Neurology Initial
        (553, 797),  # Telehealth PM&R Extended
        (554, 798),  # Telehealth PM&R FU
        (509, 796),  # Telehealth PM&R Initial
        (556, 800),  # Telehealth PrevCardio Extended
        (557, 801),  # Telehealth PrevCardio FU
        (555, 799),  # Telehealth PrevCardio Initial
        (551, 805),  # Chiropractic Training 1:1 30 minute
        (561, 806),  # Appointment type: Covid PCR
        (566, 809),  # Rapid COVID Test
        (567, 448),  # Medical IV
        (565, 829),  # Pediatric Integrative Initial 75 min
        (564, 830),  # Pediatric Integrative Extended
        (563, 831),  # Pediatric Integrative Followup
        (569, 832),  # Telehealth Pediatric Initial
        (570, 833),  # Telehealth Pediatric Extended
        (571, 834),  # Telehealth Pediatric Followup
        (573, 849),  # Precision Nutrition Webinar
        (574, 850),  # Ozone Wound Care
        (580, 889),  # Care Coordination (30 imn)
        (581, 890),  # Care Coordination (60 min)
        (597, 909),  # ALA IV
        (600, 929),  # IV Blood Draw
        (601, 949),  # Chiropractic Active Release
        (606, 969),  # EV Injection MB ID
        (607, 971),  # EV Nebulized MB ID
        (608, 970),  # EV IV MB ID
        (611, 989),  # Executive Mental Health Consult
        (612, 1009),  # Program Medical Intake 90 minutes
        (613, 1010),  # Program Medical Check-in 30 minutes
        (614, 1011),  # Program Medical Check-in 15 minutes
        (615, 1012),  # Program Medical Review 60 minutes
    ],
    "provider_ids": [
        (8, 1),
        (9, 2),
        (5, 3),
        (11, 5),
        (100000048, 9),
        (7, 18),
        (100000015, 20),
        (25, 21),
        (29, 22),
        (100000023, 24),
        (100000068, 34),
        (100000095, 44),
        (100000194, 47),  # Jeff Reed
        (100000213, 51),
        (100000238, 53),  # Thomas Scott Jamison
        (100000198, 48),  # Emily Wills
        (100000255, 54),  # Quest B.
        # removed (100000202, 57),  # Lily Liu
        # removed (100000204, 57),  # Lily Liu (second profile)
        (100000271, 59),  # Cardiac Tech
        (100000281, 62),  # Maryam Naemi
        (100000285, 48),  # Emily Guilfoy (Second profile)
        (100000298, 67),  # Intake Paperwork
        (9, 2),  # Dr. Rathod
        (100000095, 44),  # Cynthia
        (25, 21),  # David Trybus
        (100000285, 48),  # Emily Guilfoy
        (100000322, 71),  # Amanda Peregrin
        (100000323, 72),  # Christen Rincker
        (100000325, 73),  # physician Karrie Hohn
        (100011807, 10847),  # Joseph Sheehan
    ],
}

NJ = {
    "mindbody_site_id": "770930",
    "athena_practice_id": "19434",
    "timezone": "US/Eastern",
    "app_type_ids": [
        (12, 49),  # Acupuncture Initial / 60 minutes
        (13, 50),  # Acupuncture Follow up / 60 minutes
        (61, 42),  # Functional Medicine Extended / 60 minutes
        (62, 43),  # Functional Medicine Follow up / 45 minutes
        (68, 41),  # Functional Medicine Initial / 75 minutes
        (70, 45),  # Primary Care Follow up / 45 minutes
        (69, 44),  # Primary Care Initial / 60 minutes
        (104, 47),  # Cardiology New / 30 minutes — F&P Cardiology Initial
        (105, 48),  # Cardiology Follow up / 30 minutes — F&P Cardiology Follow Up
        (102, 61),  # Functional Gastroenterology Initial 75 minutes
        (103, 62),  # Functional Gastroenterology Extended 60 minutes
        (1112, 46),  # Acute Visit / 30 minutes — Primary Care Acute
        (1113, 63),  # Functional Gastroenterology follow up 45 minutes
        (1139, 81),  # Neurology Initial
        (1140, 82),  # Neurology Extended
        (1111, 121),  # Restorative Sleep Consult
        (77, 54),  # IV NUTRITION
        (78, 55),  # IV CHELATION HEALTH HEALTH
        (79, 56),  # IV CHELATION HEAVY METALS
        (1136, 141),  # IV Consult
        (74, 52),  # MEDICAL ASSISTANT VISIT
        (75, 51),  # NURSE VISIT
        (76, 53),  # LAB VISIT
        (1144, 101),  # CIMT Procedure
        (1163, 181),  # Psychiatry/Psychotherapy Initial
        (1165, 182),  # Psychiatry/Psychotherapy Follow Up 60
        (1164, 183),  # Psychiatry/Psychotherapy Follow Up 30
    ],
    "provider_ids": [
        (100000046, 5),  # Giovanni Campanile
        (100000047, 2),  # Victoria Rand
        (100000048, 4),  # Rekha Mandel
        (100000049, 3),  # Edward Barbarito
        (3, 1),  # Sita Kedia
        (100000010, 9),  # Bernard Chan
        (100000017, 10),  # Jieru Sun
        (100000051, 6),  # Lauren Munsch
        (100000102, 11),  # Amy Gonzalez
        (100000118, 12),  # Cardiac Tech
        (100000119, 13),  # Bianca Chiara
        (100000058, 7),  # IVRN
        (100000057, 8),  # MA/RN
        (100000125, 15),  # Nigel Lester
    ],
}
