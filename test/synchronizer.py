import random
import json

from unittest.mock import MagicMock
from datetime import datetime, timedelta
from collections import OrderedDict

from core import synchronizer, storage
from core.test import PalmTestCase
from core.apis import mindbody, athena

from test.fixtures import fake_client, fake_appointment_medical


today = datetime.today()
f_today = today.strftime('%Y-%m-%d')
f_lastweek = (today - timedelta(days=30)).strftime('%Y-%m-%d')

__all__ = ['SynchronizerTest']


class SynchronizerTest(PalmTestCase):
    def fake_appointment_sync(self, mb_appt):
        # Fake client
        client = fake_client()
        client['ClientSystemID'] = mb_appt['SystemID']
        client['LastName'] = mb_appt['LastName']
        storage.create_patient(client, 1)
        # Fake provider
        storage.create_provider(mb_appt['StaffID'], 86,
                                firstname=mb_appt['EmployeeFirst'],
                                lastname=mb_appt['EmployeeLast'])

    def test_is_syncable_appointment(self):
        func = synchronizer.is_syncable_appointment
        self.assertFalse(func({}))
        self.assertFalse(func({'VisitTypeID': None}))
        self.assertFalse(func({'VisitTypeID': '105'}))
        self.assertFalse(func({'VisitTypeID': '999', 'StaffID': '999'}))
        self.assertFalse(func({'VisitTypeID': '105', 'StaffID': '999'}))
        self.assertTrue(func({'VisitTypeID': '105', 'StaffID': '8'}))

    def test_sync_created_patients(self):
        # Retrieve from MindBody
        client = mindbody.retrieve_created_clients(f_lastweek, f_today,
                                                   limit=1)[0]
        # Check client doesn't exist yet in the database
        patient = storage.patients.find_one(
            mindbody_id=client['ClientSystemID']
        )
        self.assertEqual(patient, None)
        # Import clients
        synchronizer.sync_created_patients(f_lastweek, f_today, limit=1)
        patient = storage.patients.find_one(
            mindbody_id=client['ClientSystemID']
        )
        self.assertTrue(int(patient['mindbody_id']) > 0)
        patient_dict = json.loads(patient['athena_data'])
        if client['ClientMiddleName'] is None:
            self.assertEqual(patient_dict['middlename'], '')
        else:
            self.assertEqual(client['ClientMiddleName'],
                             patient_dict['middlename'])
        self.assertEqual(patient_dict['suffix'], client['Prefix'] or '')

    def test_sync_updated_patients(self):
        # Retrieve from MindBody
        client = mindbody.retrieve_updated_clients(f_lastweek, f_today,
                                                   limit=1)[0]
        original_client_lastname = client['ClientLastName']
        client['ClientLastName'] = 'madeup'
        # Create the client in the database
        storage.create_patient(client, 7357)
        patient = storage.patients.find_one(
            mindbody_id=client['ClientSystemID']
        )
        self.assertEqual(
            json.loads(patient['mindbody_data'])['ClientLastName'], 'madeup'
        )
        # Import updated clients
        synchronizer.sync_updated_patients(f_lastweek, f_today, limit=1)
        patient = storage.patients.find_one(
            mindbody_id=client['ClientSystemID']
        )
        # client in the DB should be updated
        self.assertEqual(
            json.loads(patient['mindbody_data'])['ClientLastName'],
            original_client_lastname
        )

    def test_sync_created_appointments(self):
        # Retrieve appointment from MindBody
        # Prepare mock
        fake_appointment = SynchronizerTest.fake_retrieve_created_appointment()
        mindbody.retrieve_created_appointments = MagicMock(
            return_value=[fake_appointment])
        mb_appt = fake_appointment
        appt_id = mb_appt['ApptID']
        self.fake_appointment_sync(mb_appt)
        # Import appointments
        synchronizer.sync_created_appointments(f_lastweek, f_today, limit=1)
        appointment = storage.appointments.find_one(mindbody_id=appt_id)
        self.assertIsNotNone(appointment)
        self.assertTrue(appointment['athena_id'] > 0)

        athena_appt = athena.get_appointment(appointment['athena_id'])
        self.assertEqual(int(athena_appt['appointmentid']),
                         appointment['athena_id'])

    def test_sync_updated_appointments(self):
        # Setup the test for update
        # Prepare mock
        fake_appointment = SynchronizerTest.fake_retrieve_created_appointment()
        self.fake_appointment_sync(fake_appointment)

        # Create in database a fake_appointment
        athena_raw_data = athena.appointment_from_mindbody(fake_appointment)
        athena_data = athena.export_appointment_format(athena_raw_data)
        self.assertEqual(athena_data['bookingnote'], 'Ends at: 2:30PM')
        appointment_id = athena.create_appointment(athena_data)
        storage.create_appointment(fake_appointment, athena_data,
                                   appointment_id)

        # Test if the appointment is updated
        fake_appointment = SynchronizerTest.fake_retrieve_updated_appointment()
        mindbody.retrieve_updated_appointments = MagicMock(
            return_value=[fake_appointment])

        appt_id = fake_appointment['ApptID']
        synchronizer.sync_updated_appointments(f_lastweek, f_today, limit=1)
        appointment = storage.appointments.find_one(mindbody_id=appt_id)
        self.assertIsNotNone(appointment)
        self.assertTrue(appointment['athena_id'] > 0)
        self.assertIn('2/12/2018 12:00:00 AM', appointment['mindbody_data'])

        athena_appt = athena.get_appointment(appointment['athena_id'])
        self.assertEqual(int(athena_appt['appointmentid']),
                         appointment['athena_id'])
        self.assertEqual(athena_appt['date'], '02/12/2018')

    def test_sync_updated_appointments_delayed_of_fifteen_minutes(self):
        # Setup the test for update
        # Prepare mock
        fake_appointment = SynchronizerTest.fake_retrieve_created_appointment()
        self.fake_appointment_sync(fake_appointment)

        # Create in database a fake_appointment
        athena_raw_data = athena.appointment_from_mindbody(fake_appointment)
        athena_data = athena.export_appointment_format(athena_raw_data)
        appointment_id = athena.create_appointment(athena_data)
        storage.create_appointment(fake_appointment, athena_data,
                                   appointment_id)

        # Test if the appointment is updated
        fake_appointment = SynchronizerTest.fake_retrieve_created_appointment()
        # VisitTime was 12/30/1899 1:45:00 PM
        fake_appointment['VisitTime'] = '12/30/1899 1:30:00 PM'
        # VisitEnd was 12/30/1899 2:30:00 PM
        fake_appointment['VisitEnd'] = '12/30/1899 2:15:00 PM'
        mindbody.retrieve_updated_appointments = MagicMock(
            return_value=[fake_appointment])
        mb_appointment_update = mindbody.retrieve_updated_appointments(
            f_lastweek, f_today, limit=1)[0]

        appt_id = mb_appointment_update['ApptID']
        synchronizer.sync_updated_appointments(f_lastweek, f_today, limit=1)
        appointment = storage.appointments.find_one(mindbody_id=appt_id)
        self.assertIsNotNone(appointment)
        self.assertTrue(appointment['athena_id'] > 0)

        athena_appt = athena.get_appointment(appointment['athena_id'])
        self.assertEqual(int(athena_appt['appointmentid']),
                         appointment['athena_id'])
        self.assertEqual(athena_appt['date'], '02/10/2018')
        self.assertEqual(athena_appt['starttime'], '13:30')

    def test_sync_updated_appointments_change(self):
        # Setup the test for update
        # Prepare mock
        fake_appointment = SynchronizerTest.fake_retrieve_created_appointment()
        self.fake_appointment_sync(fake_appointment)

        # Create in database a fake_appointment
        athena_raw_data = athena.appointment_from_mindbody(fake_appointment)
        athena_data = athena.export_appointment_format(athena_raw_data)
        appointment_id = athena.create_appointment(athena_data)
        storage.create_appointment(fake_appointment, athena_data,
                                   appointment_id)

        # Test if the appointment is updated
        fake_appointment = SynchronizerTest.fake_retrieve_created_appointment()
        # VisitTime was 12/30/1899 1:45:00 PM
        fake_appointment['VisitTime'] = '12/30/1899 1:15:00 PM'
        # VisitTypeID was 110
        fake_appointment['VisitTypeID'] = '116'
        # StaffID was 25
        fake_appointment['StaffID'] = '100000023'
        mindbody.retrieve_updated_appointments = MagicMock(
            return_value=[fake_appointment])
        mb_appointment_update = mindbody.retrieve_updated_appointments(
            f_lastweek, f_today, limit=1)[0]

        appt_id = mb_appointment_update['ApptID']
        synchronizer.sync_updated_appointments(f_lastweek, f_today, limit=1)
        appointment = storage.appointments.find_one(mindbody_id=appt_id)

        self.assertIsNotNone(appointment)
        self.assertTrue(appointment['athena_id'] > 0)

        athena_appt = athena.get_appointment(appointment['athena_id'])
        self.assertEqual(int(athena_appt['appointmentid']),
                         appointment['athena_id'])
        self.assertEqual(athena_appt['date'], '02/10/2018')
        self.assertEqual(athena_appt['starttime'], '13:15')
        self.assertEqual(int(athena_appt['appointmenttypeid']), 102)
        self.assertEqual(int(athena_appt['providerid']), 24)

    def test_sync_canceled_appointments(self):
        # Setup the test for update
        # Prepare mock
        fake_appointment = SynchronizerTest.fake_retrieve_created_appointment()
        self.fake_appointment_sync(fake_appointment)

        # Create in database a fake_appointment
        athena_raw_data = athena.appointment_from_mindbody(fake_appointment)
        athena_data = athena.export_appointment_format(athena_raw_data)
        appointment_id = athena.create_appointment(athena_data)
        storage.create_appointment(fake_appointment, athena_data,
                                   appointment_id)

        # Test if the appointment is canceled
        fake_appointment = (
            SynchronizerTest.fake_retrieve_canceled_appointment())
        mindbody.retrieve_updated_appointments = MagicMock(
            return_value=[fake_appointment])

        appt_id = fake_appointment['ApptID']
        synchronizer.sync_updated_appointments(f_lastweek, f_today, limit=1)
        appointment = storage.appointments.find_one(mindbody_id=appt_id)
        self.assertIsNotNone(appointment)
        self.assertTrue(appointment['athena_id'] > 0)

        athena_appt = athena.get_appointment(appointment['athena_id'])
        self.assertEqual(int(athena_appt['appointmentid']),
                         appointment['athena_id'])
        self.assertEqual(athena_appt['appointmentstatus'], 'x')

    @staticmethod
    def fake_retrieve_created_appointment():
        return OrderedDict([('StudioID', '334223'), ('RegionID', '917'),
                            ('row', '1'), ('RSSID', 'PH10437'),
                            ('SystemID', '2403'), ('FirstName', 'Grace'),
                            ('LastName', 'Williams'),
                            ('VisitDate', '2/10/2018 12:00:00 AM'),
                            ('VisitTime', '12/30/1899 1:45:00 PM'),
                            ('VisitEnd', '12/30/1899 2:30:00 PM'),
                            ('ApptID', '28929'), ('VisitTypeID', '110'),
                            ('VisitTypeName', 'Chelation for Heavy Metals'),
                            ('EmployeeFirst', 'David T.'),
                            ('EmployeeLast', 'Trybus'), ('StaffID', '25'),
                            ('ProviderID', None),
                            ('LocationName', 'PALM Health'),
                            ('PricingOptionApplied', '44117'),
                            ('PricingOptionName', 'Integrative Chiropractic '
                                                  'Care Initial Visit'),
                            ('ServiceCategory', 'Complementary Medicine'),
                            ('EarlyCancel', 'No'), ('LateCancel', 'No'),
                            ('Confirmed', 'No'), ('SignedIn', 'Yes'),
                            ('Completed', 'Yes'),
                            ('MethodBooked', 'Business Mode'),
                            ('BookedDate', '2/10/2018 10:30:49 AM'),
                            ('LastModified', '2/21/2018 4:45:03 PM'),
                            ('CancelDate', None),
                            ('VisitNotes', "per Trybus; didn't know how to "
                                           "charge- SEL")])

    @staticmethod
    def fake_retrieve_updated_appointment():
        fake_appointment = \
            SynchronizerTest.fake_retrieve_created_appointment()
        fake_appointment['VisitDate'] = '2/12/2018 12:00:00 AM'
        fake_appointment['LastModified'] = '2/22/2018 4:45:03 PM'
        return fake_appointment

    @staticmethod
    def fake_retrieve_canceled_appointment():
        fake_appointment = \
            SynchronizerTest.fake_retrieve_created_appointment()
        fake_appointment['EarlyCancel'] = 'Yes'
        fake_appointment['CancelDate'] = '2/22/2018 4:45:03 PM'
        return fake_appointment
