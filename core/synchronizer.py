from core.apis import mindbody, athena
from core import storage  # , logging
from core import config


# (MB appt ID, Athena appt ID)
app_type_ids = config.site_config["app_type_ids"]

provider_ids = config.site_config["provider_ids"]


def is_syncable_appointment(mb_data):
    """
    Checks if an appointment should be synchronized based on its appointment
    type and the prodiver.
    """
    has_type = int(mb_data.get("VisitTypeID") or "0") in dict(app_type_ids).keys()
    has_provider = int(mb_data.get("StaffID") or "0") in dict(provider_ids).keys()
    return has_type and has_provider


def sync_created_patients(since, until, limit=None):
    created_ids = []
    failed_ids = []
    for client in mindbody.retrieve_created_clients(since, until, limit=limit):
        # prevent duplicates
        if storage.patients.find_one(mindbody_id=client["ClientSystemID"]):
            print("> Skip existing patient", client["ClientLastName"])
            continue
        athena_data = athena.patient_from_mindbody(client)
        try:
            athena_id = athena.create_patient(athena_data)
            patient_id = storage.create_patient(client, athena_id)
            created_ids.append(patient_id)
            print(
                "exported created patient %s (%s)"
                % (athena_id, client["ClientLastName"])
            )
        except Exception as e:
            print(
                "Error creating client. MB ID: {}. {}".format(
                    client["ClientSystemID"], e
                )
            )
            failed_ids.append(client["ClientSystemID"])

    return created_ids, failed_ids


def sync_updated_patients(since, until, limit=None):
    created_ids = []
    updated_ids = []
    failed_created_ids = []
    failed_updated_ids = []

    for client in mindbody.retrieve_updated_clients(since, until, limit=limit):
        athena_data = athena.patient_from_mindbody(client)
        patient = storage.patients.find_one(mindbody_id=client["ClientSystemID"])

        # update
        if patient:
            print("> Update patient", client["ClientLastName"])
            try:
                athena.update_patient(patient["athena_id"], athena_data)
                storage.update_patient(patient["id"], client)
                updated_ids.append(patient["id"])
            except Exception as e:
                print(
                    "Error updating patient. MB ID: {}. Athena ID: {}. {}".format(
                        client["ClientSystemID"], patient["athena_id"], e
                    )
                )
                failed_created_ids.append(client["ClientSystemID"])

        # create
        else:
            try:
                print("> Crupdate patient", client["ClientLastName"])
                athena_id = athena.create_patient(athena_data)
                patient_id = storage.create_patient(client, athena_id)
                created_ids.append(patient_id)
            except Exception as e:
                print(
                    "Error crupdating patient. MB ID: {}. {}".format(
                        client["ClientSystemID"], e
                    )
                )
                failed_created_ids.append(client["ClientSystemID"])

    return created_ids, updated_ids, failed_created_ids, failed_updated_ids


def _create_appointment(mb_data):
    mb_data["VisitTypeID"] = int(mb_data["VisitTypeID"])

    print(
        "> Create appointment {} (type {})".format(
            mb_data["ApptID"], mb_data["VisitTypeID"]
        )
    )
    athena_raw_data = athena.appointment_from_mindbody(mb_data)
    athena_data = athena.export_appointment_format(athena_raw_data)
    athena_id = athena.create_appointment(athena_data)
    appointment_id = storage.create_appointment(mb_data, athena_data, athena_id)
    return appointment_id


def sync_created_appointments(since, until, limit=None):
    created_ids = []
    failed_ids = []

    for appt in mindbody.retrieve_created_appointments(since, until, limit=limit):
        appt_type_id = appt.get("VisitTypeID")
        appt_id = appt.get("ApptID")
        # Only some appointments need to be synchronized
        if not is_syncable_appointment(appt):
            print("> Skip appointment type {} for {}.".format(appt_type_id, appt_id))
            continue
        # Avoid duplicates
        if storage.appointments.count(mindbody_id=appt_id):
            print("> Skip existing appointment {}.".format(appt_id))
            continue
        try:
            appointment_id = _create_appointment(appt)
            created_ids.append(appointment_id)
        except Exception as e:
            print("Error creating appointment. MB ID: {}. {}".format(appt_id, e))
            failed_ids.append(appt_id)

    return created_ids, failed_ids


def sync_updated_appointments(since, until, limit=None):
    failed_ids = []
    for appointment in mindbody.retrieve_updated_appointments(
        since, until, limit=limit
    ):
        stored_appointment_id = None
        appt_id = appointment.get("ApptID")
        appt_type_id = appointment.get("VisitTypeID")
        # Only some appointments need to be synchronized
        if not is_syncable_appointment(appointment):
            print("> Skip appointment type {} for {}.".format(appt_type_id, appt_id))
            continue
        stored_appointment = storage.appointments.find_one(mindbody_id=appt_id)
        # Appointment does not actually exist yet
        if not stored_appointment:
            try:
                stored_appointment_id = _create_appointment(appointment)
            except Exception as e:
                print(
                    "Error crupdating appointment. MB ID: {}".format(
                        appointment["ApptID"], e
                    )
                )
                failed_ids.append("MB {}".format(appointment["ApptID"]))
            continue

        try:
            athena_id = stored_appointment["athena_id"]
            athena_raw_data = athena.appointment_from_mindbody(appointment)
            athena_data = athena.export_appointment_format(athena_raw_data)
            # Cancel the appointment
            if appointment["CancelDate"]:
                cancel_date = mindbody.date_from_string(appointment.get("CancelDate"))
                athena_cancel_date = cancel_date.strftime("%m/%d/%Y")
                athena.cancel_appointment(athena_id, athena_cancel_date)
                storage.delete_appointment(stored_appointment["id"])
            # Update the appointment
            else:
                appt_id = athena.update_appointment(athena_id, athena_data)
                storage.update_appointment(
                    stored_appointment["id"], appointment, athena_data, appt_id
                )
        except Exception as e:
            print(
                "Error updating appointment. MB ID: {}. Athena ID: {}. {}".format(
                    appointment["ApptID"], athena_id, e
                )
            )
            failed_ids.append(stored_appointment_id)
    return failed_ids
