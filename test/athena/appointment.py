import random
import datetime
import unittest

from core import storage
# avoid import name collision mindbody <> mindbody_api
from core.apis import athena, mindbody as mindbody_api
from core.test import PalmTestCase

from test.fixtures import (
    fake_appointment_wellbeing as fake_mindbody_appointment
)


def generic_appointment_data():
    return {
        'appointmentdate': '08/18/2022',
        'appointmenttime': '02:45',
        'bookingnote': 'Ends at: 1:30PM',
        'appointmenttypeid': '2',
        'providerid': '87',
        'appointmenttypeid': 2,
        'providerid': 87,
        'patientid': 2,
        'departmentid': '1',
    }


class AthenaAppointmentTest(PalmTestCase):

    """
    def test_export_appointment_to_athena(self):
        providers.insert({
            'firstname': 'Terry',
            'lastname': 'Ahmad',
            'athena_id': 86
        })
        patients.insert({
            'mindbody_id': '8888'
        })
        appointment = generate_appointment_data()
        athena_id = athena.export_appointment(appointment)
        self.assertTrue(athena_id > 0)

        athena_appointment = athena.get_appointment(athena_id)
        self.assertEqual(athena_appointment['providerid'], '86')
        self.assertEqual(athena_appointment['date'], '12/21/2015')
        self.assertEqual(athena_appointment['starttime'], '15:30')
        self.assertEqual(athena_appointment['duration'], '30')

    def test_export_appointments_with_providers(self):
        synchronizer.sync_providers()
        patients.insert({
            'zenoti_id': '0ad0e732-87d2-440e-902b-e3ac1d38a3d3'
        })
        appointment = generate_appointment_data()
        athena_id = athena.export_appointment(appointment)
        athena_appointment = athena.get_appointment(athena_id)

        self.assertEqual(athena_appointment['providerid'], '86')
    """

    def test_appointment_from_mindbody(self):
        data = fake_mindbody_appointment()
        storage.create_patient({
            'ClientLastName': 'Doe',
            'ClientFirstName': 'John',
            'ClientSystemID': data['SystemID']
        }, 1886)
        storage.create_provider(mindbody_id=data['StaffID'], athena_id=4,
                                firstname='John', lastname='Doe')
        appt = athena.appointment_from_mindbody(data)
        self.assertEqual(appt['patientid'], 1886)
        self.assertTrue('appointmentid' not in appt)
        self.assertEqual(appt['duration'], 30)
        self.assertEqual(appt['date'], '05/19/2022')
        self.assertEqual(appt['starttime'], '08:00')
        self.assertEqual(appt['departmentid'], 1)
        self.assertEqual(appt['appointmenttype'], 'Coaching')
        self.assertEqual(appt['appointmenttypeid'], None)
        self.assertEqual(appt['appointmentstatus'], 'f')
        self.assertEqual(appt['providerid'], 71)

    def test_get_appointment_slots(self):
        slots = athena.get_appointment_slots({'providerid': 72})
        self.assertEqual(len(slots), 0)

    def test_get_appointment_slot(self):
        slot = athena.get_appointment_slot(659286)  # already in Athena
        self.assertEqual(slot['date'], '02/10/2022')

    def test_get_appointment(self):
        appointment = athena.get_appointment(690310)
        self.assertEqual(appointment['appointmentid'], '690310')
        self.assertEqual(appointment['patientid'], '205')

    def test_create_appointment_slot(self):
        now = datetime.datetime.utcnow() + datetime.timedelta(days=1)
        data = {
            'appointmentdate': now.strftime('%m/%d/%Y'),
            'appointmenttime': now.strftime('%H:%M'),
            'appointmenttypeid': '2',
            'providerid': '86',
        }
        slot_id = athena.create_appointment_slot(data)
        self.assertTrue(slot_id > 0)
        slot = athena.get_appointment_slot(slot_id)
        self.assertEqual(slot['date'], data['appointmentdate'])

    def test_create_appointment(self):
        now = datetime.datetime.utcnow()
        date = now.strftime('%m/%d/%Y')
        time = now.strftime('%H:%M')
        data = {
            'appointmentdate': date,
            'appointmenttime': time,
            'appointmenttypeid': '2',
            'providerid': '87',
            'appointmenttypeid': 2,
            'providerid': 87,
            'bookingnote': 'Ends at: 1:30PM',
            'patientid': 2,
        }
        appointment_id = athena.create_appointment(data)
        self.assertTrue(appointment_id > 0)
        appointment = athena.get_appointment(appointment_id)
        self.assertEqual(appointment['date'], date)
        self.assertEqual(appointment['starttime'], time)
        self.assertEqual(appointment['providerid'], '87')
        self.assertEqual(appointment['appointmenttypeid'], '2')
        self.assertEqual(appointment['duration'], '10')
        self.assertEqual(appointment['appointmentstatus'], 'f')

    def test_create_appointment_booking(self):
        data = {
            'appointmentdate': '10/10/2022',
            'appointmenttime': '10:10',
            'appointmenttypeid': '2',
            'providerid': '87',
        }
        slot_id = athena.create_appointment_slot(data)
        appointment_id = athena.create_appointment_booking(
                patient_id=7584,
                slot_id=slot_id,
                type_id=2,
                booking_note='Ends at: 1:30PM')
        self.assertTrue(appointment_id > 0)
        appointment = athena.get_appointment(appointment_id)
        self.assertEqual(appointment['patientid'], '7584')
        self.assertEqual(appointment['providerid'], '87')

    def test_cancel_appointment(self):
        appt_id = athena.create_appointment(generic_appointment_data())
        cancelled = athena.cancel_appointment(appt_id)
        self.assertTrue(cancelled)
        appointment = athena.get_appointment(appt_id)
        self.assertEqual(appointment['appointmentstatus'], 'x')

    def test_rescedule_appointment(self):
        data = generic_appointment_data()
        data['appointmenttime'] = '10:00'
        appt_id = athena.create_appointment(data)
        appointment = athena.get_appointment(appt_id)
        self.assertEqual(appointment['starttime'], '10:00')
        data['appointmenttime'] = '10:15'
        appt_id = athena.update_appointment(appt_id, data)
        resceduled = athena.get_appointment(appt_id)
        self.assertEqual(resceduled['starttime'], '10:15')

    @unittest.skip
    def test_delete_appointment(self):
        data = generic_appointment_data()
        # Create appointment
        appointment_id = athena.create_appointment(data)
        self.assertTrue(appointment_id > 0)
        # delete appointment
        athena.delete_appointment(appointment_id)
        appointment = athena.get_appointment(appointment_id)
        self.assertEqual(appointment, None)

    @unittest.skip('Is this method still useful?')
    def test_get_appointments_for_patient(self):
        existing = athena.get_appointments_for_patient(3)

        data = {
            'patientid': 3,
            'appointmentdate': '08/08/2022',
            'appointmenttime': '08:08',
            'appointmenttypeid': 2,
            'providerid': 72,
        }
        athena.create_appointment(data)
        self.assertEqual(len(athena.get_appointments_for_patient(3)),
                         len(existing) + 1)
