from datetime import datetime, timedelta

from core import storage
from core.test import PalmTestCase
from core.apis import mindbody
from core import synchronizer

today = datetime.today()
f_today = today.strftime('%Y-%m-%d')
f_lastweek = (today - timedelta(days=30)).strftime('%Y-%m-%d')


class StorageTest(PalmTestCase):
    def test_search_patient(self):
        # Retrieve from MindBody
        client = mindbody.retrieve_created_clients(f_lastweek, f_today,
                                                   limit=1)[0]
        client_first_name = client['ClientFirstName']
        client_last_name = client['ClientLastName']
        # Import client
        synchronizer.sync_created_patients(f_lastweek, f_today, limit=1)
        # Retrieve client in database
        client_storage = storage.patients.find_one()
        result = storage.search_patient(client_storage, client_first_name,
                                        client_last_name)
        self.assertEqual(result, True)
