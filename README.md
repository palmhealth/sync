# PALM Health synchronization tool

The purpose of this app is to synchronize data from the 3rd party
Spa/club/booking application MindBody to AthenaHealth application.
The data that are synchronized are the patients and the appointments.

> This is not a web server/client project but a headlesss server script to run
> synchronizations.


## Installing

> You need Python 3.x, ideally with a virtualenv.

    python3 -m venv ../venv
    source ../venv/bin/activate
    pip install -r requirements.txt


## Settings

Application settings and environments are config in core/config.py

Settings that are specific to a site are in core/site_config.py


## Running the application

```
PY_ENV=dev ATHENA_KEY=<athena-key> ATHENA_SECRET=<athena-secret> python app.py --site NJ
```

Will run the synchronization with the predeploy settings (can also be _predeploy_ or _prod_)
with the _athena_key_ and _athena-secret_ for the site _New Jersey_ (could also be _STL_).


## Running tests

    python -m unittest test


## Syncing process

The core functionalities are in _core/synchronizer.py_.

MindBody's _clients_ are retrieved in this order: created, updated (and deleted, though it may have stopped working on Athena).
They are saved as local _Patients_.
Patients are then exported to Athena's first department and retrieves extra
infos that are updated locally and send to the second department.
(Note this is not implemented, we don't have an access to associate
one patient through different departments using the API).

We then do the same for appointments.

> Athena has multiple _departments_ each being isolated.
PALM requires to be able to read a patient's profile or an appointment from any
_department_.
Patients and appointments belonging to one department only they must therefore
be recorded once into each Athena _department_.
