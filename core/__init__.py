from . import storage


class Logger:
    def error(self, message, data=None, forced=None):
        self.data = data
        self.forced = forced
        print(message)
        if data:
            pass # storage.create_failure(data, forced)


logging = Logger()
