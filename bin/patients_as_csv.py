#!/usr/bin/env python
import sys
import os

sys.path.append(os.path.dirname(__file__) + '/../')

import json
import csv

from core.storage import patients


columns = ['firstname', 'middlename', 'lastname', 'sex', 'dob', 'email',
           'homephone', 'mobilephone', 'address1', 'address2', 'zip', 'city',
           'state', 'countrycode3166']

outputs = []
for patient in patients.all():
    data = json.loads(patient['athena_data'])
    row = []
    for prop in columns:
        row.append(data[prop])
    outputs.append(row)

target = open('patients.csv', 'w')
writer = csv.writer(target)
writer.writerows([columns] + outputs)
