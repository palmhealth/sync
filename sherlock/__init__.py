'''
Extra module for investigating on Palm data.
'''

import datetime

from core.apis import athena, mindbody
from core import storage


def find_client_appointments(lastname, date=None):
    '''
    Find all appointments events from MindBody for a client for a day.
    '''
    if not date:
        date = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d')

    created = mindbody.retrieve_created_appointments(date, date)
    updated = mindbody.retrieve_updated_appointments(date, date)
    return {
        'created': [a for a in created if a['LastName'] == lastname],
        'updated': [a for a in updated if a['LastName'] == lastname]
    }


def cancel_appointment(mb_appt):
    '''
    takes a MindBody appointment and cancels it on Athena.
    '''
    stored = storage.appointments.find_one(mindbody_id=mb_appt['ApptID'])
    return athena.cancel_appointment(stored['athena_id'])
