import unittest

from core.apis.athena import (
    patient_from_mindbody, create_patient, find_patients, get_patient,
    update_patient, delete_patient
)
from core.test import PalmTestCase
from core.utils import ucfirst

from test.fixtures import fake_client


def generate_athena_patient():
    '''
    Generates a fake patient with Athena format.
    '''
    return patient_from_mindbody(fake_client())


class AthenaPatientTest(PalmTestCase):
    def test_patient_from_mindbody(self):
        client = fake_client()
        client['ClientFirstName'] = 'Jane'
        client['ClientBirthDate'] = '01/05/1961'
        client['Gender'] = 'Female'
        client['ClientNickname'] = None
        patient = patient_from_mindbody(client)
        self.assertEqual(patient['firstname'], 'Jane')
        self.assertEqual(patient['dob'], '01/05/1961')
        self.assertEqual(patient['sex'], 'f')
        self.assertEqual(patient['middlename'], '')

    def test_create_patient(self):
        data = generate_athena_patient()
        patient_id = create_patient(data)  # patientid does not exist
        exported = get_patient(patient_id)

        self.assertEqual(exported['countrycode3166'], data['countrycode3166'])
        self.assertEqual(len(exported['countrycode']), 3)
        self.assertEqual(exported['address1'], ucfirst(data['address1']))

    def test_update_patient(self):
        data = generate_athena_patient()
        patient_id = create_patient(data)
        new_data = {**data, **{'dob': '12/25/1980'}}
        update_patient(patient_id, new_data)
        patient = get_patient(patient_id)
        self.assertEqual(patient['lastname'], data['lastname'])
        self.assertEqual(patient['dob'], new_data['dob'])

    def test_unretrievable_patient_is_added_in_athena(self):
        client = fake_client()
        client['ClientHomePh'] = ''
        client['ClientCell'] = ''
        client['ClientWorkPh'] = ''
        client['ClientZip'] = ''
        # Test if ClientEmail's key is empty
        client['ClientEmail'] = ''
        patient = patient_from_mindbody(client)
        self.assertEqual(patient['email'], '_')
        # Test if ClientEmail's key is None
        client['ClientEmail'] = None
        patient = patient_from_mindbody(client)
        self.assertEqual(patient['email'], '_')
        client_id = create_patient(patient)
        self.assertTrue(int(client_id) > 0)

    @unittest.skip('Delete a user API may have been suppressed.')
    def test_delete_patient(self):
        '''
        This action seems to have been deactivated. No reply about this from
        Athena, no changelog.
        '''
        data = generate_athena_patient()
        patient_id = create_patient(data)
        delete_patient(patient_id)
        self.assertIsNone(get_patient(patient_id))

    def test_find_patients(self):
        data = generate_athena_patient()
        patient_id = create_patient(data)
        patients = find_patients({'lastname': data['lastname'], 'firstname': data['firstname']})
        self.assertEqual(len(patients), 1)

    def test_suffix_patient(self):
        data = generate_athena_patient()
        patient_id = create_patient(data)
        patient = get_patient(patient_id)
        self.assertEqual(data['suffix'], patient['suffix'])
