download_database:
	rsync -avz athens:~/palmhealth_sync/database-* .

deploy:
	ssh athens 'cd ~/palmhealth_sync/ && \
							git fetch origin master && \
							git reset --hard FETCH_HEAD && \
							source ./venv/bin/activate && \
							pip install -r requirements.txt'
