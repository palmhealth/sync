import unittest

from core.utils import ucfirst, extract_us_number


class UtilsTest(unittest.TestCase):

    def test_ucfirst(self):
        self.assertEqual(ucfirst('test me'), 'Test Me')
        self.assertEqual(ucfirst('TeST mE'), 'Test Me')
        self.assertEqual(ucfirst('a b C de'), 'A B C De')
        self.assertEqual(ucfirst('do-do'), 'Do-do')

    def test_extract_us_number(self):
        self.assertEqual(extract_us_number('6363337888'), '6363337888')
        self.assertEqual(extract_us_number('16363337888'), '6363337888')
        self.assertEqual(extract_us_number('+16363337888'), '6363337888')
        self.assertEqual(extract_us_number('+1 (636) 333-7888'), '6363337888')
        self.assertEqual(extract_us_number('+1 (636) 333-8888'), None)
        self.assertEqual(extract_us_number('+1 (636) 333-8888 5'), None)
        self.assertEqual(extract_us_number('+33 6 12345678'), None)
        self.assertEqual(extract_us_number('abc'), None)
        self.assertEqual(extract_us_number(''), None)

if __name__ == '__main__':
    unittest.main()
