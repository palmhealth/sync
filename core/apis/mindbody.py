from datetime import datetime

import xmltodict
from suds.client import Client

from core import config


mb_config = config.MINDBODY[config.ENV]



# --- Monkey patch
import suds
def mindbody_process_reply(self, reply, *args, **kwargs):
    '''
    MindBody has an invalid XML Response Check (`TypeNotFound` error).
    Therefore I dirty monkeypatch the Suds method to skip all checks and return
    the XML raw response directly.
    '''
    return reply
suds.client.SoapClient.process_reply = mindbody_process_reply
# ---


def date_from_string(string):
    if not string:
        return None
    pattern = '%m/%d/%Y %I:%M:%S %p' if len(string) > 10 else '%m/%d/%Y'
    return datetime.strptime(string, pattern)


def date_to_string(datetime):
    if not datetime:
        return ''
    if datetime.hour or datetime.minute or datetime.second:
        pattern = '%m/%d/%Y %I:%M:%S %p'
    else:
        pattern = '%m/%d/%Y'
    return datetime.strftime(pattern)


def query(function, action, startdate, enddate, **kwargs):
    limit = kwargs.get('limit') or 1000
    soap = Client(url=mb_config['wsdl_url'], location=mb_config['soap_url'])
    request = soap.factory.create('FunctionDataXml').Request

    sourceCredentials = soap.factory.create('SourceCredentials')
    sourceCredentials.SourceName = mb_config['source_name']
    sourceCredentials.Password = mb_config['password']
    sourceCredentials.SiteIDs.int = mb_config['site_ids']

    request.FunctionName = function
    request.CurrentPageIndex = 0
    request.PageSize = 1000
    request.XMLDetail = 'Full'

    functionParams = soap.factory.create('ArrayOfFunctionParam')
    minrow = soap.factory.create('FunctionParam')
    minrow.ParamName = '@minrow'
    minrow.ParamValue = 0
    minrow.ParamDataType = 'string'
    functionParams.FunctionParam = minrow
    maxrow = soap.factory.create('FunctionParam')
    maxrow.ParamName = '@maxrow'
    maxrow.ParamValue = limit
    maxrow.ParamDataType = 'string'
    start = soap.factory.create('FunctionParam')
    start.ParamName = '@{action}start'.format(action=action)
    start.ParamValue = startdate
    start.ParamDataType = 'datetime'
    end = soap.factory.create('FunctionParam')
    end.ParamName = '@{action}end'.format(action=action)
    end.ParamValue = enddate
    end.ParamDataType = 'datetime'
    functionParams.FunctionParam = [minrow, maxrow, start, end]
    request.SourceCredentials = sourceCredentials
    request.FunctionParams = functionParams

    reply = soap.service.FunctionDataXml(request)
    parsed = xmltodict.parse(reply)

    data_xml_response = (parsed['soap:Envelope']['soap:Body']
                         ['FunctionDataXmlResponse'])
    results = data_xml_response['FunctionDataXmlResult'].get('Results')
    return results['Row'] if results else []


def retrieve_created_clients(start, end, *args, **kwargs):
    pattern = '%Y-%m-%d %H:%M:%S'
    if len(start) <= 10:
        start += ' 00:00:00'
    if len(end) <= 10:
        end += ' 23:59:59'
    since = date_to_string(datetime.strptime(start, pattern))
    until = date_to_string(datetime.strptime(end, pattern))
    results = query('PalmHealth_ClientInfoV2', 'create', since, until, *args,
                    **kwargs)
    return results if isinstance(results, list) else [results]


def retrieve_updated_clients(start, end, *args, **kwargs):
    pattern = '%Y-%m-%d %H:%M:%S'
    if len(start) <= 10:
        start += ' 00:00:00'
    if len(end) <= 10:
        end += ' 23:59:59'
    since = date_to_string(datetime.strptime(start, pattern))
    until = date_to_string(datetime.strptime(end, pattern))
    results = query('PalmHealth_ClientInfoV2', 'mod', since, until, *args,
                    **kwargs)
    return results if isinstance(results, list) else [results]


def get_appointment_duration(appointment):
    time = date_from_string(appointment['VisitTime'])
    end_time = date_from_string(appointment['VisitEnd'])
    return round((end_time - time).seconds / 60)


def retrieve_created_appointments(start, end, *args, **kwargs):
    pattern = '%Y-%m-%d %H:%M:%S'
    if len(start) <= 10:
        start += ' 00:00:00'
    if len(end) <= 10:
        end += ' 23:59:59'
    since = date_to_string(datetime.strptime(start, pattern))
    until = date_to_string(datetime.strptime(end, pattern))
    results = query('PalmHealth_ApptInfoV2', 'creation', since, until, *args,
                    **kwargs)
    return results if isinstance(results, list) else [results]


def retrieve_updated_appointments(start, end, *args, **kwargs):
    pattern = '%Y-%m-%d %H:%M:%S'
    if len(start) <= 10:
        start += ' 00:00:00'
    if len(end) <= 10:
        end += ' 23:59:59'
    since = date_to_string(datetime.strptime(start, pattern))
    until = date_to_string(datetime.strptime(end, pattern))
    results = query('PalmHealth_ApptInfoV2', 'modified', since, until, *args,
                    **kwargs)
    return results if isinstance(results, list) else [results]
