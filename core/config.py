"""
This file manage all configuration variables.
It also have a notion of few environments such as 'dev', 'prod', and
'predeploy' (for importing real 'prod' data and exporting to 'dev' env)
"""
import os
from os.path import dirname, abspath
from importlib import import_module
from argparse import ArgumentParser
from os import environ


parser = ArgumentParser()
parser.add_argument(
    "--site", dest="site", help="Name of the site to sync (STL or NJ)", required=False, default=environ.get('SITE')
)
args = parser.parse_args()

getenv = os.environ.get

try:
    from .private_config import *
except ImportError:
    ATHENA_KEY = getenv("ATHENA_KEY")
    ATHENA_SECRET = getenv("ATHENA_SECRET")

if not ATHENA_KEY or not ATHENA_SECRET:
    raise Exception("No Athena API key or secret was set.")

ENV = getenv("PY_ENV", "dev")
ROOT_PATH = dirname(dirname(abspath(__file__)))
SITE_NAME = args.site.upper().strip()
site_config = getattr(import_module("core.site_config"), SITE_NAME)


DATABASE = {
    "dev": {"path": "sqlite:///{0}/database-dev-{1}.db".format(ROOT_PATH, SITE_NAME)},
    "prod": {"path": "sqlite:///{0}/database-{1}.db".format(ROOT_PATH, SITE_NAME)},
}
DATABASE["predeploy"] = DATABASE["dev"]

ATHENA = {
    "dev": {
        "token_url": "https://api.preview.platform.athenahealth.com/oauth2/v1/token",
        "url": "https://api.preview.platform.athenahealth.com/v1/",
        "key": ATHENA_KEY,
        "secret": ATHENA_SECRET,
        "department_id": "1",
        "practice_id": "195900",
    },
    "prod": {
        "token_url": "https://api.platform.athenahealth.com/oauth2/v1/token",
        "url": "https://api.platform.athenahealth.com/v1/",
        "key": ATHENA_KEY,
        "secret": ATHENA_SECRET,
        "department_id": "1",  # medical: 1, mental health: 21
        "practice_id": site_config["athena_practice_id"],
    },
}
ATHENA["predeploy"] = ATHENA["dev"]

MINDBODY = {
    "prod": {
        "soap_url": "https://api.mindbodyonline.com/0_5/DataService.asmx",
        "wsdl_url": "https://api.mindbodyonline.com/0_5/DataService.asmx?WSDL",
        "source_name": "Scopyleft",
        "password": "MxChaRjWxAJgpa4VAC2ELU92PHM=",
        "site_ids": [site_config["mindbody_site_id"]],
    }
}
MINDBODY["dev"] = MINDBODY["prod"]
MINDBODY["predeploy"] = MINDBODY["prod"]
